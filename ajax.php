<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Ajax</title>
    <link rel="stylesheet" href="assets/css/style.css">
</head>
<body>
<div id="example">
    <button class="switch">Click Me</button>
</div>
<div id="result">
</div>

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
<script src="assets/js/ajax.js"></script>
</body>
</html>
