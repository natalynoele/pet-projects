<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>DOM manipulation</title>
    <link rel="stylesheet" href="assets/css/style.css">
</head>
<body>
<div id="example" class="container">
    <input type="text" />
    <button>Add</button>
</div>
<div id="places-container">
    <div class="item">Madrid
        <div class="remove">X
        </div>
    </div>
    <div class="item">Rome
        <div class="remove">X</div></div>
    <div class="item">New York
        <div class="remove">X</div></div>
    <div class="item">Lublin
        <div class="remove">X</div></div>
</div>


<script src="assets/js/jquery_3.4.1.js"></script>
<script src="assets/js/events.js"></script>
</body>
</html>
