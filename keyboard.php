<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Events</title>
    <link rel="stylesheet" href="assets/css/style.css">
</head>
<body>
<div id="example" class="container">
    <input type="text" />
</div>
<div id="answer" class="answer">

</div>

<script src="assets/js/jquery_3.4.1.js"></script>
<script src="assets/js/events.js"></script>
</body>
</html>