<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>JQuery & Ajax</title>
    <link rel="stylesheet" href="assets/css/style.css">
</head>
<body>
<div id="animals" class="creature">Animals
    <div class="creature">Mammals
        <div class="creatures">Dogs</div>
        <div id="cats" class="creatures">Cats</div>
        <div class="terrain">Forest</div>
        <div class="terrain">Prairie</div>
    </div>
    <div class="creature">Reptiles
        <div class="creatures">Snakes</div>
        <div class="creatures">Lizards</div>
        <div class="terrain">Jungle</div>
    </div>
</div>
<script src="assets/js/jquery_3.4.1.js"></script>
<script src="assets/js/dom.js"></script>
</body>
</html>
