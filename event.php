<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Events</title>
    <link rel="stylesheet" href="assets/css/style.css">
</head>
<body>
<div id="example" class="container">
    <select id="place">
        <option value="">Pick your destination...</option>
        <option value="kyiv" data-price="320">Kyiv</option>
        <option value="vinnytsia" data-price="270">Vinnytsia</option>
        <option value="lviv" data-price="230">Lviv</option>
        <option value="kharkiv" data-price="190">Kharkiv</option>
        <option value="odesa" data-price="280">Odesa</option>
    </select>
    <button>Pick</button>
</div>
<div id="answer" class="answer">

</div>

<script src="assets/js/jquery_3.4.1.js"></script>
<script src="assets/js/events.js"></script>
</body>
</html>