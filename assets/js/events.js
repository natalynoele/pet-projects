$(document).ready(function () {
    $('#example').on('click', 'button.switch', function () {
        $(this).parent().toggleClass('highlighted');
    });

    // $('#example').on('click', 'button', function () {
    //     var selected = $('#place option:selected');
    //     var value  = selected.val();
    //     var price = selected.data('price');
    //     if (price) {
    //         $('#answer').html(value + ' ' + price);
    //     }
    // });

    //change event
    // $('#example').on('change', 'select', function () {
    //     var selected = $(this).find('option:selected');
    //     var value = selected.val();
    //     var price = selected.data('price');
    //     if (price){
    //         $('#answer').html(value + ' ' + price);
    //     }
    // });

    //keyboard events
    //keydown
    // $('#example').on('keydown', 'input', function () {
    //     $('#answer').html('Your name is: ' + $(this).val());
    // });
// keyup
//     $('#example').on('keyup', 'input', function () {
//         $('#answer').html('Your name is: ' + $(this).val());
//     });


    //dom manipulation
    $('#example').on('click', 'button', function () {
        var city = $('#example input').val();
        var html = '<div class="item">'+ city +
            '    <div class="remove">X</div></div>';

        // $('#places-container').append(html);
        // $(html).appendTo('#places-container');

        // $('#places-container').prepend(html);

        //before aftrer
        // $('#places-container').children().last().after(html);
        $('#places-container').children().first().before(html);
        
        //remove this doesn't work for new elements
        // $('#places-container .remove').on('click', function () {
        //     $(this).parent().remove();
        // })


        $('#places-container').on('click', '.remove', function () {
            $(this).parent().remove();
        })

        
    });
})