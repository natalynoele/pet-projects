$(document).ready(function () {
    //traversing the DOM - find()

//    css selectors
    var selector = $('#animals .creature');
    // console.log(selector);


  //DOM traversing (more efficient)
    selector = $('#animals').find('.creature');
    // console.log(selector);

    //select descendants
//    css selectors
    var children = $('#animals > .creature');
    // console.log(children);

//    DOM traversing
    children = $('#animals').children('.creature');
    // console.log(children);

    //first last
    var firstChildren = $('#animals').children().first().children('.creatures').last();
    // console.log(firstChildren);

    //previously  next


    //parent
    var parent = $('#cats').parent();
    // console.log(parent);


    //parents

    var parents = $('#cats').parents();
    console.log(parents);

});